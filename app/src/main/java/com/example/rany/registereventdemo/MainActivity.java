
package com.example.rany.registereventdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnAnno, btnImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAnno = findViewById(R.id.btnAnno);
        btnImpl = findViewById(R.id.btnImpl);
        btnImpl.setOnClickListener(this);

        btnAnno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Anonymouse Inner class", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Implement from View Listener", Toast.LENGTH_SHORT).show();
    }

    public void viewClick(View view) {
        Toast.makeText(this, "On View Click", Toast.LENGTH_SHORT).show();
    }
}
